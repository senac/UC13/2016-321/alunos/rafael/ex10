/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.megaxhost.ex10;

/**
 *
 * A classe abaixo tem um exemplo de acoplamento. Só lembrando que acoplamento é uma relação entre duas classes. 
Nesse caso, motor está acoplado a classe carro e vice-versa o motor e o coração do carro sem ele o carro não
* tem função nenhuma e apenas um monte de lata que não vai se mover.
 */
public class Carro {
  private Motor motor;
    public Carro(Motor motor) {
       if (motor == null) throw new NullPointerException();
       this.motor = motor;
    }
}