/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.megaxhost.ex10;

/**
 *
 * @author MegaxHost Coesao: A classe deve ter apenas uma única 
 * responsabilidade e a classe não deve assumir responsabilidades que não são suas
 * a classe Programa tem responsabilidades que não são suas, 
 * como obter um produto e gravá-lo no banco de dados. Então,
 * dizemos que esta classe não está coesa, ou seja, ela tem 
 * responsabilidades demais, e o que é pior, responsabilidades que não são suas.
 */
public class Programa {
     public void ExibirFormulario()    {
         //implementação
     }
  
     public void ObterProduto()    {
         //implementação
     }
  
     public void gravarProdutoDB   {
         //implementação
     }
}
